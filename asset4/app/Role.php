<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	//eloquent relationship // one to many // roles to users
    public function users()
    {
    	return $this->hasMany('\App\User');
    }
}
