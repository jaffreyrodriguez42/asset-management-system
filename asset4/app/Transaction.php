<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    // eloquent relationship // transactions to users, statuses, assets and categories // many to one
    public function user()
    {
        return $this->belongsTo('\App\User');
    }

    public function status()
    {
        return $this->belongsTo('\App\Status');
    }

    public function asset()
    {
        return $this->belongsTo('\App\Asset');
    }

    public function category()
    {
        return $this->belongsTo('\App\Category');
    }
}
