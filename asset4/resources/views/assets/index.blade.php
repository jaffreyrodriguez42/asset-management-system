@extends('layouts.app')

@section('content')
	{{-- assets index page just for admin --}}
    <div class="card-header">
        <h5>Assets</h5>
    </div>


    <div class="card-body row col-md-12 custyle">
    <table class="table table-striped custab">
    <thead>
    <a href="#" class="btn btn-primary btn-xs pull-right mt-3"><b>+</b> Add new asset</a>
        <tr>
            <th>Product Line</th>
            <th>Serial Number</th>
            <th>Status</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($assets as $asset)
            <tr>
            	<td class="align-middle"><a href="/assets/{{$asset->id}}">{{$asset->category->name}}</a></td>
                <td class="align-middle">{{$asset->serial_number}}</td>
                <td class="align-middle">
					@if($asset->isAvailable == 1)
					{{"Available"}}
					@else
					{{"Unavailable"}}
					@endif
                </td>

                <td class="text-center align-middle">
                	<a href=""{{-- "/assets/{{$asset->id}}/edit" --}} class='btn btn-warning btn-xs pl-5 pr-5 d-inline-block'><span class="glyphicon glyphicon-edit"></span> Edit</a>
                	<div class="d-inline-block">
                		<form method="POST" action="/assets/{{$asset->id}}">
                			@csrf
                			@method('DELETE')
                			@if($asset->isAvailable == 1)
                			<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Make Unavailable</button>
                			@else
                			<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-remove"></span> Make Available</button>
                			@endif
                		</form>
                	</div>
                </td>
            </tr>
      	@endforeach
      </tbody>
    </table>
    {{ $assets->render() }}
    </div>

@endsection