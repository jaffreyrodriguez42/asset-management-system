<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'isAvailable' => 1,
                'serial_number' => 'E-EBG1',
                'created_at' => '2020-05-01 13:06:19',
                'updated_at' => '2020-05-01 13:11:20',
                'category_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'isAvailable' => 1,
                'serial_number' => 'E-EBG2',
                'created_at' => '2020-05-02 03:58:44',
                'updated_at' => '2020-05-02 03:58:44',
                'category_id' => 2,
            ),
        ));
        
        
    }
}