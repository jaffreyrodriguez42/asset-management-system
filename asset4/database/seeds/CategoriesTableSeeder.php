<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Epson EB-L1100',
                'description' => 'Nice Projector',
                'img_path' => 'images/1588338370.png',
                'isActive' => 1,
                'created_at' => '2020-05-01 13:06:10',
                'updated_at' => '2020-05-01 13:06:10',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Epson EB-2042',
                'description' => 'Educational and Business',
                'img_path' => 'images/1588391895.png',
                'isActive' => 1,
                'created_at' => '2020-05-02 03:58:15',
                'updated_at' => '2020-05-02 03:58:15',
            ),
        ));
        
        
    }
}