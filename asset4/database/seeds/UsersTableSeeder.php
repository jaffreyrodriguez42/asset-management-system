<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Jaff',
                'email' => 'jhan_172005@yahoo.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$ULnGiYf3qKsSxSLJlaEJXOwMZG4PQgwfO8qBTWVTm5SvGOx0OQgrq',
                'remember_token' => NULL,
                'created_at' => '2020-04-30 12:21:06',
                'updated_at' => '2020-04-30 12:21:06',
                'role_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Fe',
                'email' => 'jaffreyrodriguez42@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$0vmomTIYTZ0TWj3xY8QfN.3qH2Bp9/1s51qiSmfrqXi3LAm7AGk7W',
                'remember_token' => NULL,
                'created_at' => '2020-04-30 12:22:12',
                'updated_at' => '2020-04-30 12:22:12',
                'role_id' => 2,
            ),
        ));
        
        
    }
}